<?php

/**
 * @file
 * Definition of views_handler_field_node_link_delete.
 */

/**
 * Field handler to present a link to delete a node.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_node_link_delete_ajax extends views_handler_field_node_link {

  /**
   * Renders the link.
   */
  function render_link($node, $values) {
    // Ensure user has access to delete this node.
    if (!node_access('delete', $node)) {
      return;
    }

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['path'] = "node/$node->nid/delete_ajax/nojs";
    $this->options['alter']['query'] = drupal_get_destination();

    ctools_include('modal');
    ctools_modal_add_js();
    drupal_add_js(drupal_get_path('module', 'vbo_ajax_confirm') . '/js/vbo_ajax_modal.js');
    drupal_add_js(array(
      'CToolsModal' => array(
        'modalTheme' => 'VBO_ModalDialog',
        'modalSize' => array(
          'type' => 'fixed',
          'width' => 460,
          'height' => 250,
          'contentBottom' => 800),
      ),
    ), 'setting');

    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
    return $text;
  }
}
