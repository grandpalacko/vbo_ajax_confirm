(function ($) {
  // Make sure our objects are defined.
  Drupal.VBO_Modal = Drupal.VBO_Modal || {};

  Drupal.VBO_Modal.vbo_modal_display = function(ajax, response, status) {
    if ($('#modalContent').length == 0) {
      Drupal.CTools.Modal.show(Drupal.CTools.Modal.getSettings(ajax.element));
    }
    // Simulate an actual page load by scrolling to the top after adding the
    // content. This is helpful for allowing users to see error messages at the
    // top of a form, etc.
    $('#vbo-modal-content').html(response.output).scrollTop(0);
    $('#vbo-modal-close').attr('href', window.location.href);

    // Attach behaviors within a modal dialog.
    var settings = response.settings || ajax.settings || Drupal.settings;
    Drupal.attachBehaviors('#modalContent', settings);

    if ($('#vbo-modal-content').hasClass('ctools-modal-loading')) {
      $('#vbo-modal-content').removeClass('ctools-modal-loading');
    }
    else {
      // If the modal was already shown, and we are simply replacing its
      // content, then focus on the first focusable element in the modal.
      // (When first showing the modal, focus will be placed on the close
      // button by the show() function called above.)
      $('#modal-content :focusable:first').focus();
    }
  }

  /**
   * Handler to prepare the modal for the response
   */
  Drupal.VBO_Modal.clickAjaxLink = function () {
    Drupal.CTools.Modal.show(Drupal.CTools.Modal.getSettings(this));
    return false;
  };

  /**
   * Provide the HTML to create the modal dialog.
   */
  Drupal.theme.prototype.VBO_ModalDialog = function () {
    var html = ''
    html += '  <div id="ctools-modal" class="modal">'
    html += '    <div class="ctools-modal-content">' // panels-modal-content
    html += '      <div class="modal__header u-cf">';
    html += '        <a id="vbo-modal-close" class="modal__close" href="#">';
    html +=            'close';
    html += '        </a>';
    html += '      </div>';
    html += '      <div id="vbo-modal-content" class="modal__content">';
    html += '      </div>';
    html += '    </div>';
    html += '  </div>';

    return html;
  }

  $(function() {
    Drupal.ajax.prototype.commands.vbo_modal_display = Drupal.VBO_Modal.vbo_modal_display;
  });
})(jQuery);